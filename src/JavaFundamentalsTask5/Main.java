package JavaFundamentalsTask5;

import java.lang.reflect.Array;
import java.util.Scanner;

//   Ввести число от 1 до 12. Вывести на консоль название месяца, соответствующего данному числу. Осуществить проверку корректности ввода чисел.
public class Main {

    public static void main(String[] args) {
        // делаем ввод через консоль
        System.out.println("Введите число от 1 до 12");
        Scanner inputNumber = new Scanner(System.in);
        int numberOfMonth = inputNumber.nextInt();
        if (numberOfMonth < 1 || numberOfMonth > 12) {
            System.out.println("Вы вели неподхожящее число - "+"\"" + numberOfMonth +"\""+ ", нужно ввести число от 1 до 12 !!!");
        } else {
            switch (numberOfMonth) {
                case 1:
                    System.out.println("Это январь");
                    break;
                case 2:
                    System.out.println("Это февраль");
                    break;
                case 3:
                    System.out.println("Это март");
                    break;
                case 4:
                    System.out.println("Это апрель");
                    break;
                case 5:
                    System.out.println("Это май");
                    break;
                case 6:
                    System.out.println("Это июнь");
                    break;
                case 7:
                    System.out.println("Это июль");
                    break;
                case 8:
                    System.out.println("Это август");
                    break;
                case 9:
                    System.out.println("Это сентябрь");
                    break;
                case 10:
                    System.out.println("Это окрябрь");
                    break;
                case 11:
                    System.out.println("Это ноябрь");
                    break;
                case 12:
                    System.out.println("Это декабрь");
                    break;
            }
        }
        if (numberOfMonth==1||numberOfMonth==2||numberOfMonth==12) {
            System.out.println("Сезон - Зима");
        } else {
            if (numberOfMonth >= 3 && numberOfMonth <= 5) {
                System.out.println("Сезон - Весна");
            } else {
                if (numberOfMonth >= 6 && numberOfMonth <= 8) {
                    System.out.println("Сезон - Лето");
                } else {
                    if (numberOfMonth >= 9 && numberOfMonth <= 11) {
                        System.out.println("Сезон - Осень");
                    }
                }

            }
        }
    }
}

